/*
* Write a method that takes in a String and returns the reversed version of the String.
* Examples:
* reverseString("abcde") -> "edcba"
* reverseString("1") -> "1"
* reverseString("") -> ""
* reverse("madam") -> "madam"
* reverse(null) -> null
*/
package com.firecode.levelone.exc4;

class ReverseString {
    public String reverseString(String str){
        if(str == null){
            return null;
        }

        StringBuffer outputString = new StringBuffer();
        for(int i = str.length() - 1; i >= 0 ; --i){
            outputString.append(str.charAt(i));
        }

        return outputString.toString();
    }
}