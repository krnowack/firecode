package com.firecode.levelone.exc1;


public class DeleteAListTailNode {

    public ListNode deleteAtTail(ListNode head) {
        if(null == head || null == head.next){
            return null;
        }

        head.next = deleteAtTail(head.next);
        return head;
    }
}
