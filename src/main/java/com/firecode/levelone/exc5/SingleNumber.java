package com.firecode.levelone.exc5;

import java.util.HashMap;
import java.util.Map;

/*
 * Write a method that returns a number that appears only once in an array.
 * Assume the array will surely have a unique value. The array will never be empty.
 * {1,2,3,4,1,2,4,3,5} ==> 5
 */
public class SingleNumber {
   
    public int singleNumber(int[] A) {
        
        Map<Integer, Integer> kvs = new HashMap<>();
        
        for(int item : A){
            if(kvs.containsKey(item)){
                Integer value = kvs.get(item);
                kvs.put(item, value +1);
            } else{
                kvs.put(item, 1);
            }
        }

        return kvs.entrySet()
            .stream()
            .filter(e-> e.getValue() == 1)
            .findFirst()
            .map(Map.Entry::getKey)
            .orElse(Integer.MIN_VALUE);
    }
}
