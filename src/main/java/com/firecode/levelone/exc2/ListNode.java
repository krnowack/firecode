package com.firecode.levelone.exc2;

public class ListNode {
    int data;
    ListNode next;
    ListNode(int data) { this.data = data; }

    @Override
    public String toString() {
        if(this.next != null){
            return this.data + " => " + this.next;
        }

        return Integer.toString(this.data);
    }
}
