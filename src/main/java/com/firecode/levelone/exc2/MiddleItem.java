/*
Given a Singly-Linked List, write a method - findMiddleNode that finds and returns the middle node of the list in a single pass.

Examples:
1 ==> 1
1->2 ==> 1
1->2->3->4 ==> 2
1->2->3->4->5 ==> 3
*/

package com.firecode.levelone.exc2;

public class MiddleItem {
    public ListNode findMiddleNode(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode middleItem = head;
        ListNode farItem = head.next;

        while (farItem != null && farItem.next != null) {
            middleItem = middleItem.next;
            farItem = farItem.next.next;
        }

        return middleItem;
    }
}
