/*
* Given an Array containing 9 numbers ranging from 1 to 10, write a method to find the missing number.
* Assume you have 9 numbers between 1 to 10 and only one number is missing.
* findMissingNumber({1,2,4,5,6,7,8,9,10}) --> 3
*/

package com.firecode.levelone.exc3;

import java.util.HashMap;
import java.util.Map;

class FindMissingNumberInASetOfNumbers {
    public int findMissingNumber(int[] arr) {
         Map<Integer, Boolean> dict = new HashMap<>();
        for(int i = 1; i <= 10; ++i){
            dict.put(i, false);
        }

        for(int item : arr){
            dict.put(item, true);
        }

        return dict.entrySet()
                .stream()
                .filter(kv-> kv.getValue() == false)
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(Integer.MIN_VALUE);
    }
}