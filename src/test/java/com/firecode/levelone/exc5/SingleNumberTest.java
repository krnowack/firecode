package com.firecode.levelone.exc5;

import org.junit.Test;
import static org.junit.Assert.*;

public class SingleNumberTest {
    @Test
    public void shouldReturnNegative1(){
        int tab[] = {1, -1, 1};
        SingleNumber classUnderTest = new SingleNumber();

        assertEquals(-1, classUnderTest.singleNumber(tab));
    }

    @Test
    public void shouldReturn7(){
        int tab[] = {1,2,3,3,2,1,7,8,8};
        SingleNumber classUnderTest = new SingleNumber();

        assertEquals(7, classUnderTest.singleNumber(tab));
    }

    @Test
    public void shouldReturn1(){
        int tab[] = {1};
        SingleNumber classUnderTest = new SingleNumber();

        assertEquals(1, classUnderTest.singleNumber(tab));
    }

    @Test
    public void shouldReturn5(){
        int tab[] = {1,1,1,4,5,6,7,7,6,4};
        SingleNumber classUnderTest = new SingleNumber();

        assertEquals(5, classUnderTest.singleNumber(tab));
    }

    @Test
    public void shouldReturnAlso5(){
        int tab[] = {1,2,3,4,1,2,4,3,5};
        SingleNumber classUnderTest = new SingleNumber();

        assertEquals(5, classUnderTest.singleNumber(tab));
    }


}
