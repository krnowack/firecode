package com.firecode.levelone.exc1;

import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DeleteAListTailNodeTest {

    @Test
    public void listNodeShouldBeNull(){
        ListNode head = null;
        DeleteAListTailNode sut = new DeleteAListTailNode();

        assertNull(sut.deleteAtTail(head));
    }

    @Test
    public void listNodeShouldBeNullIfThereIsOnlyOneElement(){
        ListNode head = new ListNode(1);
        head.next = null;

        DeleteAListTailNode sut = new DeleteAListTailNode();
        assertNull(sut.deleteAtTail(head));
    }

    @Test
    public void shouldBeOnlyOneListNodeIfLinkedListHasTwoElements(){
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);

        DeleteAListTailNode sut = new DeleteAListTailNode();
        ListNode result = sut.deleteAtTail(head);
        assertNotNull(result);
        assertEquals(1, result.data);
        assertNull(result.next);
    }

    @Test
    public void shouldBeOnlyThreListNodesIfLinkedListHasFourElements(){
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);

        DeleteAListTailNode sut = new DeleteAListTailNode();
        ListNode result = sut.deleteAtTail(head);
        assertNotNull(result);
        assertEquals(1, result.data);

        assertNotNull(result.next);
        assertEquals(2, result.next.data);

        assertNotNull(result.next.next);
        assertEquals(3, result.next.next.data);

        assertNull(result.next.next.next);
    }

}
