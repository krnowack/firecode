package com.firecode.levelone.exc4;

import org.junit.Test;
import static org.junit.Assert.*;

public class ReverseStringTest {
    @Test
    public void shouldReturnNull() {
        String testData = null;
        ReverseString sut = new ReverseString();
        assertNull(sut.reverseString(testData));
    }

    @Test
    public void shouldReturnEmptyString() {
        String testData = "";
        ReverseString sut = new ReverseString();
        assertTrue(sut.reverseString(testData).isEmpty());
    }

    @Test
    public void shouldReturnReversedString() {
        String testData = "abcd";
        ReverseString sut = new ReverseString();
        assertEquals("dcba", sut.reverseString(testData));
    }

    @Test
    public void shouldReturnSameStringIfOnlyItContainsOneLetter() {
        String testData = "a";
        ReverseString sut = new ReverseString();
        assertEquals("a", sut.reverseString(testData));
    }
}
