package com.firecode.levelone.exc2;

import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MiddleItemTest {

    @Test
    public void listNodeShouldBeNull(){
        ListNode head = null;
        MiddleItem sut = new MiddleItem();

        assertNull(sut.findMiddleNode(head));
    }

    @Test
    public void listNodeShouldBeNullIfThereIsOnlyOneElement(){
        ListNode head = new ListNode(1);
        head.next = null;

        MiddleItem sut = new MiddleItem();
        ListNode result = sut.findMiddleNode(head);
        assertNotNull(result);
        assertEquals(1, result.data );

    }

//    @Test
//    public void shouldBeOnlyOneListNodeIfLinkedListHasTwoElements(){
//        ListNode head = new ListNode(1);
//        head.next = new ListNode(2);
//
//        DeleteAListTailNode sut = new DeleteAListTailNode();
//        ListNode result = sut.deleteAtTail(head);
//        assertNotNull(result);
//        assertEquals(1, result.data);
//        assertNull(result.next);
//    }

//    @Test
//    public void shouldBeOnlyThreListNodesIfLinkedListHasFourElements(){
//        ListNode head = new ListNode(1);
//        head.next = new ListNode(2);
//        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);
//
//        DeleteAListTailNode sut = new DeleteAListTailNode();
//        ListNode result = sut.deleteAtTail(head);
//        assertNotNull(result);
//        assertEquals(1, result.data);
//
//        assertNotNull(result.next);
//        assertEquals(2, result.next.data);
//
//        assertNotNull(result.next.next);
//        assertEquals(3, result.next.next.data);
//
//        assertNull(result.next.next.next);
//    }

}
