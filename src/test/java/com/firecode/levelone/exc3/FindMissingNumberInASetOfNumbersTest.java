
package com.firecode.levelone.exc3;

import org.junit.Test;
import static org.junit.Assert.*;

public class FindMissingNumberInASetOfNumbersTest{

    @Test
    public void shouldReturn3(){
        int testData [] = {1,2,4,5,6,7,8,9,10};
        FindMissingNumberInASetOfNumbers sut = new FindMissingNumberInASetOfNumbers();
        assertEquals(3, sut.findMissingNumber(testData));
    }

    @Test
    public void shouldReturn9(){
        int testData [] = {4,5,6,7,8,10,1,2,3};
        FindMissingNumberInASetOfNumbers sut = new FindMissingNumberInASetOfNumbers();
        assertEquals(9, sut.findMissingNumber(testData));
    }

    @Test
    public void shouldReturnAnother9(){
        int testData [] = {1,2,3,4,5,6,7,8,10};
        FindMissingNumberInASetOfNumbers sut = new FindMissingNumberInASetOfNumbers();
        assertEquals(9, sut.findMissingNumber(testData));
    }

    @Test
    public void shouldReturnAnother3(){
        int testData [] = {6,7,8,9,10,1,2,4,5};
        FindMissingNumberInASetOfNumbers sut = new FindMissingNumberInASetOfNumbers();
        assertEquals(3, sut.findMissingNumber(testData));
    }
}